console.log('Request Data...');

const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('Preparing Data...');
        const backendData = {
            server: 'aws',
            port: 2000,
            status: 'Pending'
        }
        resolve(backendData)
    }, 2000);
})
.then(data => {
    return new Promise((resolve) => {
        setTimeout(() => {
            data.status = 'Received';
            console.log('Data Received', data);
        }, 2000);
    });
});


// Fetch Data Request

const fetchData = fetch('https://jsonplaceholder.typicode.com/users').then(response => response.json()).then(data => console.log(data))


// Asyn Await

async function asyncAwaitFunc () {
    console.log('Fetch Async Await Started...');
    const response = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await response.json();
    console.log('Data Received', data);
}

// asyncAwaitFunc();